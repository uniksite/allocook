@extends('layouts.app')

@section('title')
    Search Engine
@endsection

@section('style')
    .responsive-img {
    width: 100%;
    border-radius: 10px;
    }

    .search-recipe-box {
    background: white;
    -webkit-box-shadow: 0 0 5px 2px #d8d8d8;
    -moz-box-shadow: 0 0 5px 2px #d8d8d8;
    box-shadow: 0 0 5px 2px #d8d8d8;
    border-radius: 10px;
    }

    .padding-10 {
    padding: 10px;
    }
@endsection

@section('content')
    <?php
    $nbPages = ceil($recipes->count/50);
    if ($nbPages > 15) {
        $nbPages = 15;
    }
    ?>
    <div class="row">
        <ul class="pagination center-align">
            <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
            <?php for ($page = 1; $page <= $nbPages; $page++) : ?>
            <li class="waves-effect <?= $page == $currentpage ? 'active blue darken-3' : '' ?>"><a href="search?q=<?= $q ?>&page=<?= $page ?>"><?= $page ?></a></li>
            <?php endfor; ?>
            <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
        </ul>
    </div>
    <div class="row">
        <?php foreach ($recipes->hits as $hit) : ?>
        <div class="col s12 m4 l4 xl3">
            <div class="padding-10">
                <div class="search-recipe-box">
                    <div class="padding-10">
                        <?php
                        try {
                            list($recipeUrl, $recipeId) = explode('_', $hit->recipe->uri);
                        } catch (Exception $e) {
                            $recipeId = 0;
                        }
                        ?>
                        <a href="/recipe?r=<?= strrev($recipeId) ?>">
                            <img class="responsive-img" src='<?= $hit->recipe->image ?>' />
                        </a>
                    </div>
                    <div class="padding-10">
                        <a href="/recipe?r=<?= strrev($recipeId) ?>">
                            <?= $hit->recipe->label ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
    <div class="row">
        <ul class="pagination center-align">
            <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
            <?php for ($page = 1; $page <= $nbPages; $page++) : ?>
            <li class="waves-effect <?= $page == $currentpage ? 'active blue darken-3' : '' ?>"><a href="search?q=<?= $q ?>&page=<?= $page ?>"><?= $page ?></a></li>
            <?php endfor; ?>
            <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
        </ul>
    </div>
@endsection
