@extends('layouts.app')

@section('title')
    Search Engine
@endsection

@section('style')

@endsection

@section('content')
    <div class="row">
        <div class="col s12">
            <div class="padding-10">
                <div class="search-recipe-box">
                    <div class="row">
                        <div class="col s12">
                            <div class="padding-10">
                                <div class="center-align padding-10"><b><u><?= $recipe[0]->label ?></u></b></div>
                                <div class="center-align padding-10"><img class="responsive-img" src='<?= $recipe[0]->image ?>' /></div>
                                <u>Ingredients</u>:
                                <?php foreach ($recipe[0]->ingredients as $ingredient) : ?>
                                    <div>- <?= $ingredient->text ?></div>
                                <?php endforeach; ?>
                                <br />
                                <u>Calories</u>: <?= $recipe[0]->calories ?> Cal
                                <br /><br />
                                <u>Instructions</u>:
                                <div class="instructions"><?= $recipeInstructions ?></div>
                                <br /><br />
                                <u>Source</u>: <a href="<?= $recipe[0]->url ?>" target="_blank">Click here</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
