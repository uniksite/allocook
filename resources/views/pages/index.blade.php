@extends('layouts.app')

@section('title')
    Allo Cook, all recipes in one place and more
@endsection

@section('style')
    .responsive-img {
        width: 100%;
        border-radius: 10px;
    }

    .search-recipe-box {
        background: white;
        -webkit-box-shadow: 0 0 5px 2px #d8d8d8;
        -moz-box-shadow: 0 0 5px 2px #d8d8d8;
        box-shadow: 0 0 5px 2px #d8d8d8;
        border-radius: 10px;
    }

    .padding-10 {
        padding: 10px;
    }
@endsection

@section('content')
    <div class="row">
        @guest
        <?php foreach ($recipes->hits as $hit) : ?>
            <div class="col s12 m4 l4 xl3">
                <div class="padding-10">
                    <div class="search-recipe-box">
                        <div class="padding-10">
                            <?php
                            try {
                                list($recipeUrl, $recipeId) = explode('_', $hit->recipe->uri);
                            } catch (Exception $e) {
                                $recipeId = 0;
                            }
                            ?>
                            <a href="/recipe?r=<?= strrev($recipeId) ?>">
                                <img class="responsive-img" src='<?= $hit->recipe->image ?>' />
                            </a>
                        </div>
                        <div class="padding-10">
                            <a href="/recipe?r=<?= strrev($recipeId) ?>">
                                <?= $hit->recipe->label ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
        @else

                <div class="col s12">
                    <div class="padding-10">
                        <div class="search-recipe-box">
                            <div class="row">
                                <div class="col s12">
                                    <div class="padding-10">
                                        <h5>Dashboard</h5>

                                        @if (session('status'))
                                            <div class="alert alert-success" role="alert">
                                                {{ session('status') }}
                                            </div>
                                        @endif

                                        You are logged in {{ Auth::user()->name }}!

                                        <h5>Featured Recipes</h5>
                                        ...

                                        <h5>New Recipes</h5>
                                        ...

                                        <h5>News Feed</h5>
                                        No feed at the moment, please follow some people
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

        @endguest
    </div>
@endsection