@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col s12 m6 offset-m3">
            <div class="padding-10">
                <div class="search-recipe-box">
                    <div class="padding-10">
                        <h5>{{ __('Register') }}</h5>

                        <div class="row">
                            <form class="col s12" method="POST" action="{{ secure_url('register') }}">
                                @csrf

                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                                        <label for="name" class="blue-text darken-3">{{ __('Name') }}</label>
                                    </div>

                                    <div class="col s12">
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                                        <label for="email" class="blue-text darken-3">{{ __('E-Mail Address') }}</label>
                                    </div>

                                    <div class="col s12">
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                        <label for="password" class="blue-text darken-3">{{ __('Password') }}</label>
                                    </div>


                                    <div class="col s12">
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                        <label for="password-confirm" class="blue-text darken-3">{{ __('Confirm Password') }}</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col s12">
                                        <button type="submit" class="btn btn-primary blue darken-3">
                                            {{ __('Register') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
