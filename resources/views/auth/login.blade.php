@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col s12 m6 offset-m3">
            <div class="padding-10">
                <div class="search-recipe-box">
                    <div class="padding-10">
                        <h5>{{ __('Login') }}</h5>

                        <div class="row">
                            <form class="col s12" method="POST" action="{{ secure_url('login') }}">
                                @csrf

                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                        <label for="email" class="blue-text darken-3">{{ __('E-Mail Address') }}</label>
                                    </div>

                                    <div class="col S12">
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                        <label for="password" class="blue-text darken-3">{{ __('Password') }}</label>
                                    </div>

                                    <div class="col S12">
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col s12">

                                            <label>
                                                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                <span>{{ __('Remember Me') }}</span>
                                            </label>


                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col s12">
                                        <button type="submit" class="btn btn-primary blue darken-3">
                                            {{ __('Login') }}
                                        </button>

                                        <a class="btn btn-link blue darken-3" href="{{ secure_url('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
