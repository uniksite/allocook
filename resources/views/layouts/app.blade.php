<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    <script src="{{ secure_asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ secure_asset('css/app.css?v=1') }}" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-129769340-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-129769340-1');
    </script>
</head>
<body class="grey lighten-4">
    <style>
        #wrapper {
            padding-left: 250px;
            padding-top: 65px;
        }

        .ingredient-search {
            font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Oxygen-Sans,Ubuntu,Cantarell,Helvetica Neue,sans-serif;
            text-transform: none;
            vertical-align: unset;
            background-color: #000000;
        }

        .ingredient-search:hover {
            background-color: #082b61;
        }

        a.sidenav-trigger.top-nav i {
            font-size: 32px;
        }

        #slide-out {
            transform:translateX(0%);
            width: 250px;
        }

        .padding-10 {
            padding: 10px;
        }

        .nav-search input:focus {
            background-color: #ffffffe3 !important;
        }

        #floating-bar {
            position: fixed;
            padding-left: 250px;
            z-index: 3;
            width: 100%;
        }

        .responsive-img {
            width: 200px;
            border-radius: 10px;
        }

        .search-recipe-box {
            background: white;
            -webkit-box-shadow: 0 0 5px 2px #d8d8d8;
            -moz-box-shadow: 0 0 5px 2px #d8d8d8;
            box-shadow: 0 0 5px 2px #d8d8d8;
            border-radius: 10px;
        }

        .instructions img {
            max-width: 200px !important;
            height: auto !important;
        }

        @media screen and (max-width: 992px) {
            #wrapper {
                padding-left: 200px;
            }

            #floating-bar {
                padding-left: 200px;
            }

            #slide-out {
                width: 200px;
            }

            .sidenav li>a {
                padding: 0 20px;
            }
        }

        @media screen and (max-width: 600px) {
            #wrapper {
                padding-left: 0px;
            }

            #floating-bar {
                padding-left: 0px;
            }

            #slide-out{
                transform:translateX(-105%);
                width: 250px;
            }
        }


        @yield('style')
    </style>

    <ul id="slide-out" class="sidenav fixed">
        <li>
            <div class="user-view">
        <li>
            <a href="\">
                <i class="material-icons blue-text darken-3">home</i> Home
            </a>
        </li>
        @guest
            <li>
                <a href="{{ secure_url('login') }}">
                    <i class="material-icons blue-text darken-3">vpn_key</i> {{ __('Login') }}
                </a>
            </li>
            <li>
                @if (Route::has('register'))
                    <a href="{{ secure_url('register') }}">
                        <i class="material-icons blue-text darken-3">person_add</i> {{ __('Register') }}
                    </a>
                @endif
            </li>
        @else
            <li>
                <a href="\my-profile">
                    <i class="material-icons blue-text darken-3">person</i> My profile
                </a>

                <a href="\add-recipe">
                    <i class="material-icons blue-text darken-3">restaurant_menu</i> Add a recipe
                </a>

                <a href="\shopping-list">
                    <i class="material-icons blue-text darken-3">view_list</i> Shopping list
                </a>

                <a href="\activity-log">
                    <i class="material-icons blue-text darken-3">access_time</i> Activity log
                </a>

                <a href="\settings">
                    <i class="material-icons blue-text darken-3">settings</i> Settings
                </a>


                <a href="{{ secure_url('logout') }}"
                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <i class="material-icons blue-text darken-3">exit_to_app</i> {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ secure_url('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        @endguest
        <li>
            <a href="\contact">
                <i class="material-icons blue-text darken-3">contact_mail</i> Contact us
            </a>
        </li>
        </div>
        </li>
    </ul>

    <div id="floating-bar">
        <div class="row blue darken-3">
            <div style="display: table-cell; vertical-align: middle;" class="padding-10">
                <a href="#" data-target="slide-out" class="top-nav full white-text" id="switch-menu">
                    <i class="material-icons">menu</i>
                </a>
            </div>
            <div style="display: table-cell; width: 100%;">
                <nav class="nav-search" style="box-shadow: unset; background-color: #ffffff30">
                    <div class="nav-wrapper">
                        <form action="/search" method="get">
                            <div class="input-field">
                                <input id="search" type="search" placeholder="Find a recipe" name="q" autocomplete="off" required>
                                <label class="label-icon" for="search"><i class="material-icons">search</i></label>
                                <i class="material-icons">
                                    close
                                </i>
                            </div>
                        </form>
                    </div>
                </nav>
            </div>
        </div>
    </div>

    <div id="wrapper">
        <div class="row">
            <h6 class="col s12 valign-wrapper">
                <span class="show-on-small hide-on-med-and-up">&nbsp;&nbsp;</span> Allo Cook progression ... 18%
            </h6>
        </div>
        <div class="progress blue lighten-3">
            <div class="determinate blue darken-3" style="width: 18%"></div>
        </div>

        <div class="row">
            <div class="col s12">
                <!-- Page content  -->
                <div id="columns">
                    @yield('content')
                </div>
            </div>
        </div>

    </div>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.sidenav');
            var instances = M.Sidenav.init(elems, {});
        });

        var overlay = document.getElementsByClassName("sidenav-overlay");
        var sideNav = document.getElementById("slide-out");
        document.getElementById("switch-menu").addEventListener("click", function(event){
            sideNav.style.transform = sideNav.style.transform === 'translateX(-105%)' ? 'translateX(0%)' : 'translateX(-105%)';

            var sideNavWidth = document.getElementById("slide-out").offsetWidth;

            if (screen.width > 600) {
                document.getElementById("wrapper").style.paddingLeft = sideNav.style.transform === 'translateX(-105%)' ? '0px' : sideNavWidth + 'px';
                document.getElementById("floating-bar").style.paddingLeft = sideNav.style.transform === 'translateX(-105%)' ? '0px' : sideNavWidth + 'px';
            } else {
                overlay[0].style.display = sideNav.style.transform === 'translateX(-105%)' ? 'none' : 'block';
                overlay[0].style.opacity = sideNav.style.transform === 'translateX(-105%)' ? '0' : '1';
                document.body.style.overflow = sideNav.style.transform === 'translateX(-105%)' ? '' : 'hidden';
            }

            event.preventDefault();
        });

        window.onload = function() {
            overlay[0].onclick = function () {
                overlay[0].style.display = 'none';
                overlay[0].style.opacity = '0';
                document.body.style.overflow = '';

                sideNav.style.transform = sideNav.style.transform === 'translateX(-105%)' ? 'translateX(0%)' : 'translateX(-105%)';
            };

            if (screen.width <= 600) {
                sideNav.style.transform = 'translateX(-105%)';
            }
        }
    </script>
</body>
</html>
