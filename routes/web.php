<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');
Route::get('/search', 'PagesController@search');
Route::get('/recipe', 'PagesController@recipe');
Route::get('/contact', 'PagesController@contact');
Route::get('/account', 'PagesController@account');
Route::get('/my-profile', 'PagesController@addRecipe');
Route::get('/add-recipe', 'PagesController@addRecipe');
Route::get('/shopping-list', 'PagesController@shoppingList');
Route::get('/activity-log', 'PagesController@activityLog');
Route::get('/settings', 'PagesController@settings');

Route::resource('posts', 'PostsController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
