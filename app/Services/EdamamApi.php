<?php

namespace App\Services;

class EdamamApi extends AbstractApi
{
    private $baseUrl = 'https://api.edamam.com';
    private $appId = '0f8d982d';
    private $appKey = 'ae534f28e2a1765bee95b4dfe09dff30';

    public function getSearch($term, $from, $to)
    {
        $res = $this->client->request("GET", "{$this->baseUrl}/search?q=$term&app_id={$this->appId}&app_key={$this->appKey}&from=$from&to=$to");
        return $res->getBody();
    }

    public function getRecipe($recipeId) {
        $parameterR = "http%3A%2F%2Fwww.edamam.com%2Fontologies%2Fedamam.owl%23recipe_";
        $url = "{$this->baseUrl}/search?app_id={$this->appId}&app_key={$this->appKey}&r={$parameterR}{$recipeId}";
        $res = $this->client->request("GET", $url);
        return $res->getBody();
    }
}