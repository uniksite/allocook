<?php

namespace App\Services;


class AbstractApi {
    public $client;

    function __construct() {
        $this->client = new \GuzzleHttp\Client();
    }
}