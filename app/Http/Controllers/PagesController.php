<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services;

use andreskrey\Readability\Readability;
use andreskrey\Readability\Configuration;

class PagesController extends Controller
{
    public function index()
    {
        $edamam = new Services\EdamamApi();
        $recipes = json_decode($edamam->getSearch('recipe', 0, 10));
        return view('pages.index')->with('recipes', $recipes);
    }

    public function search(Request $request)
    {
        $terms = $request->input('q');
        $page= $request->input('page') ?: 1;

        $from = ($page - 1 ) * 50 + 1;
        $to = $page * 50;

        $edamam = new Services\EdamamApi();
        $recipes = json_decode($edamam->getSearch($terms, $from, $to));

        $data = [
            'q' => $terms,
            'currentpage' => $page,
            'recipes' => $recipes,
        ];
        return view('pages.search')->with($data);
    }

    public function myProfile()
    {
        return view('pages.myProfile');
    }

    public function addRecipe()
    {
        return view('pages.addRecipe');
    }

    public function shoppingList()
    {
        return view('pages.shoppingList');
    }

    public function activityLog()
    {
        return view('pages.activityLog');
    }

    public function settings()
    {
        return view('pages.settings');
    }

    public function account(Request $request)
    {
        return view('pages.account');
    }

    public function contact()
    {
        return view('pages.contact');
    }

    public function recipe(Request $request)
    {
        $recipeId = strrev($request->input('r'));
        $edamam = new Services\EdamamApi();
        $recipe = json_decode($edamam->getRecipe($recipeId));

        $readability = new Readability(new Configuration());
        $html = $this->getData($recipe[0]->url);

        if (!empty($html)) {
            $readability->parse($html);
            $html = $readability->getContent();
            $recipeInstructions = $this->formatHtmlContent($html);
        } else {
            $recipeInstructions = "";
        }

        $data = [
            'recipe' => $recipe,
            'recipeInstructions' => $recipeInstructions,
        ];
        return view('pages.recipe')->with($data);
    }

    private function formatHtmlContent($html)
    {
        $recipeInstructions = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $html);
        $recipeInstructions = str_replace("<div", "<span style='display: inline-block'", $recipeInstructions);
        $recipeInstructions = str_replace("</div>", "</span>", $recipeInstructions);
        $recipeInstructions = str_replace("<h1>", "<br><b>", $recipeInstructions);
        $recipeInstructions = str_replace("<h2>", "<br><b>", $recipeInstructions);
        $recipeInstructions = str_replace("<h3>", "<br><b>", $recipeInstructions);
        $recipeInstructions = str_replace("<h4>", "<br><b>", $recipeInstructions);
        $recipeInstructions = str_replace("<h5>", "<br><b>", $recipeInstructions);
        $recipeInstructions = str_replace("<h6>", "<br><b>", $recipeInstructions);
        $recipeInstructions = str_replace("</h1>", "</b><br>", $recipeInstructions);
        $recipeInstructions = str_replace("</h2>", "</b><br>", $recipeInstructions);
        $recipeInstructions = str_replace("</h3>", "</b><br>", $recipeInstructions);
        $recipeInstructions = str_replace("</h4>", "</b><br>", $recipeInstructions);
        $recipeInstructions = str_replace("</h5>", "</b><br>", $recipeInstructions);
        $recipeInstructions = str_replace("</h6>", "</b><br>", $recipeInstructions);
        $recipeInstructions = str_replace("<label", "<span", $recipeInstructions);
        $recipeInstructions = str_replace("</label>", "</span>", $recipeInstructions);
        $recipeInstructions = preg_replace('#<a.*?>.*?</a>#is', '', $recipeInstructions);
        $recipeInstructions = strip_tags($recipeInstructions, '<br><p><h6><span><img><b>');
        $recipeInstructions = preg_replace("/&#?[a-z0-9]+;/i","", $recipeInstructions);
        return $recipeInstructions;
    }

    private function getData($url) {
        $url = str_replace('&amp;', '&', urldecode(trim($url)) );
        $timeout = 5;
        //$cookie = tempnam('/tmp', 'CURLCOOKIE');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1');
        curl_setopt($ch, CURLOPT_URL, $url);
        //curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        $content = curl_exec($ch);
        curl_close ($ch);
        return $content;
    }
}
